<?php

namespace Workshop\Solid\Example1;

class MessageLogger
{
    /**
     * @param string $message
     */
    public static function logMessage($message)
    {
        file_put_contents('/var/log/my-app/action.log', $message . "\n", FILE_APPEND);
    }
}

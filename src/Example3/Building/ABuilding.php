<?php

namespace Workshop\Solid\Example3\Building;

use Workshop\Solid\Example3\Building\BuildingInterface;

class ABuilding implements BuildingInterface
{
    /** @var int */
    private $level;

    /**
     * @param int $level
     */
    public function __construct($level)
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return int
     */
    abstract public function getType();
}


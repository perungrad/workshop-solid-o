<?php

namespace Workshop\Solid\Example3\Building;

class BuildingType
{
    const LUMBERJACK   = 1;
    const QUARRY       = 2;
    const IRON_MINE    = 3;
    const FARMS        = 4;
    const STOREHOUSE   = 5;
}


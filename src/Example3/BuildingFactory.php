<?php

namespace Workshop\Solid\Example3;

use Workshop\Solid\Example3\Building\BuildingType;
use Workshop\Solid\Example3\Building\BuildingInterface;

class BuildingFactory
{
    /** @var array<int,string> */
    private $classes = [
        BuildingType::LUMBERJACK   => '\Workshop\Solid\Example3\Building\Lumberjack',
        BuildingType::QUARRY       => '\Workshop\Solid\Example3\Building\Quarry',
        BuildingType::IRON_MINE    => '\Workshop\Solid\Example3\Building\IronMine',
        BuildingType::FARMS        => '\Workshop\Solid\Example3\Building\Farms',
        BuildingType::STOREHOUSE   => '\Workshop\Solid\Example3\Building\Storehouse',
    ];

    /**
     * @param int $type
     * @param int $level
     *
     * @return BuildingInterface
     */
    public function create($type, $level)
    {
        if (!isset($this->classes[$type])) {
            throw new \Exception('Takýto typ budovy neexistuje');
        }

        $building = new $this->classes[$type]($level);

        return $building;
    }
}


<?php

namespace Building;

use Workshop\Solid\Example3\Building\BuildingType;
use Workshop\Solid\Example3\Building\ABuilding;

class Storehouse extends ABuilding
{
    /**
     * @return int
     */
    public function getType()
    {
        return BuildingType::STOREHOUSE;
    }
}


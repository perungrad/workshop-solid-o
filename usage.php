<?php

use Workshop\Solid\Example2\GenericEncoder;

require_once 'vendor/autoload.php';

$data = [
    'foo' => 'bar',
    'baz' => 42,
];

$genericEncoder = new GenericEncoder();
$genericEncoder->encodeToFormat($data, 'json');


<?php

namespace Building;

use Workshop\Solid\Example3\Building\BuildingType;
use Workshop\Solid\Example3\Building\ABuilding;

class Farms extends ABuilding
{
    /**
     * @return int
     */
    public function getType()
    {
        return BuildingType::FARMS;
    }
}


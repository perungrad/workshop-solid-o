<?php

namespace Workshop\Solid\Example2\Encoder;

class XmlEncoder
{
    /**
     * @param array $data
     *
     * @return string
     */
    public function encode(array $data)
    {
        $document = new \SimpleXMLElement('<root/>');

        array_walk_recursive($data, [$document, 'addChild']);

        return $document->asXML();
    }
}


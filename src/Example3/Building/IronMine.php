<?php

namespace Building;

use Workshop\Solid\Example3\Building\BuildingType;
use Workshop\Solid\Example3\Building\ABuilding;

class IronMine extends ABuilding
{
    /**
     * @return int
     */
    public function getType()
    {
        return BuildingType::IRON_MINE;
    }
}


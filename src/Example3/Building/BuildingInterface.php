<?php

namespace Workshop\Solid\Example3\Building;

interface BuildingInterface
{
    /**
     * @return int
     */
    public function getLevel();

    /**
     * @return int
     */
    public function getType();
